#include <iostream>
#include "server.h"

#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>

using namespace std;

boost::asio::io_service io_service;
boost::asio::deadline_timer timer1(io_service);


const std::string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%X", &tstruct);

    return buf;
}

void handler1(server *serv, std::string *msg)
{
    serv->BroadCast(msg->c_str());
    std::cout << "\t\t\t\t\t\t\t\t\t" << currentDateTime() << std::endl;

    timer1.expires_from_now(boost::posix_time::seconds(1));
    timer1.async_wait(boost::bind(handler1, serv, msg));

}

int main(int argc, char**argv)
{
    if(argc > 4)
    {
        std::string msg(argv[1]), path(argv[4]);

        server serv(path);
        serv.Authenticate = (bool)atoi(argv[3]);
        serv.listen(atoi(argv[2]));

        timer1.expires_from_now(boost::posix_time::seconds(1));
        timer1.async_wait(boost::bind(handler1, &serv,  &msg));

        boost::thread t(boost::bind(&boost::asio::io_service::run, &io_service));
        serv.execute();
        t.join();
    }else
        std::cout << "\n\nServer Class implementation example. \n"
                  << "The server will listen to a port and send a message every 2 seconds to the clients connected in the lan.\n\n"
                  <<  "server.exe \"message_to_be_send\" port_to_listen authenticate user_files_path \n[or]\n"
                  <<  "./server \"message_to_be_send\" port_to_listen authenticate user_files_path\n\nExample: "
                  <<  "server.exe \"Isn't R. Feynman the most greatest physicist ever?\" 3250 1 user.txt\n\n";


    return 0;
}