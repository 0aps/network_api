# Network API

# Client Class implementation example.

The client will connect to an internal address and receive it message.
If client connect argument is set to 1 the connections will be persistence
otherwise it will not. Finally, the reconnect_time is set in seconds.

client.exe server_address port username password reconnect_time connect_persistence
[or]
./client server_address port username password reconnect_time connect_persistence

Example: client.exe 192.168.1.2 3250 feynman physics 35 1

#Server Class implementation example.

The server will listen to a port and send a message every 2 seconds to the clien
ts connected in the lan.

server.exe "message_to_be_send" port_to_listen authenticate user_files_path
[or]
./server "message_to_be_send" port_to_listen authenticate user_files_path

Example: server.exe "Isn't R. Feynman the most greatest physicist ever?" 3250 1 user.txt

